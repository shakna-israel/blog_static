---
layout: post
title: Skeleton CSS
date: 2014-09-02
excerpt: Today, let me introduce you to a CSS framework that gives you a lot of freedom, mobile sensitivity, and doesn't try and think for you. The major decisions are still in your hands. The biggest bonus? No JavaScript, just CSS
---

The worst thing about building a new website from the ground up, is the CSS.

Usually, to avoid having to rewrite everything, people grab a Framework, like [Bootstrap](http://getbootstrap.com), and then tweak it to their liking.

Today, let me introduce you to a CSS framework that gives you a lot of freedom, mobile sensitivity, and doesn't try and think for you. The major decisions are still in your hands.

The biggest bonus? No JavaScript, just CSS. So, with this boilerplate, there's no browser compatibility issues to start from.

This, is [Skeleton](http://getskeleton.com), and it is powerful.

You can get Skeleton [here](www.getskeleton.com/#download) or [on GitHub](https://github.com/dhg/Skeleton).

I built [Shakna Israel](http://shaknaisrael.com/) using Skeleton, because of the way it treats every div as a block that should flow responsively, and the contents should be readable at any size.

*So, using Skeleton?*

Simple enough. After you apply the stylesheets in the usual way:

{% raw %}
```<link rel="stylesheet" href="css/base.css">```
```<link rel="stylesheet" href="css/skeleton.css">```
```<link rel="stylesheet" href="css/layout.css">```
{% endraw %}

You simple need to set up a grid, and specify widths within said grid.

So, for [Shakna Israel](http://shaknaisrael.com), I set up a 16 Column Grid:

{% raw %}
```<div class="container">```
```<div class="sixteen columns">```
```<a href="index.html" id="a_title"><h2 class="remove-bottom" style="margin-top: 40px;" id="a_title">Shakna Israel</h2></a>```
```<h5 id="a_title">Author, Programmer, Designer</h5>```
```<hr>```
```</div><!-- sixteen columns -->```
{% endraw %}

So, you have a container div, and inside it a div with the number of columns specified.

A maximum of 16 columns can be easily displayed.

Below that, each individual section of the website, for this purpose, I stored inside two-third or one-third divs.

{% raw %}
```<div class="two-third column">```
``` { content }```
```</div>```
{% endraw %}

And now, the website handles all the widths and placement auto-magickally.

Responsive, easy to use, and the site builds quick. Without the bloat of something like [Bootstrap](http://getbootstrap.com).
