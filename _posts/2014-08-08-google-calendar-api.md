---
layout: post
title: Google Calendar API
date: 2014-08-08
excerpt: Whilst I was developing for Haddon Community Learning Center, I ran into an issue. The client wanted their current events to appear on the website, but if you make a Google Calendar, and go to sharing, they give you an iframe, which looks a little like this
---

Whilst I was developing for [Haddon Community Learning Center](http://shakna-israel.gtihub.io/haddon/), I ran into an issue.

The client wanted their current events to appear on the website, but if you make a Google Calendar, and go to sharing, they give you an iframe, which looks a little like this:

{% highlight html %}
<iframe src="https://www.google.com/calendar/embed?src=[Calendar Address]&ctz=Australia/Sydney" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
{% endhighlight %}

*Where [Calendar Address] is the url Google uses to locate your calendar.*

**This looks ugly, and is not responsive**.

So in short, if I were to use that, I'd break the website.

However, if you go to [HCLC's calendar](http://shakna-israel.github.io/haddon/events), you'll see that there is an alternative, and one that can even look okay.

I pieced it together from a ton of code snippets on using the Google API, but the *most important* thing you have to pay attention to, is the Calendar Address in the iframe.

You will need that later.

*(The calendar address is just the url between **src=** and the **&**).*

**If you just want to skip the tutorial, and grab a copy of the code, head over to the [gist](https://gist.github.com/shakna-israel/6d28ef500e16579e1639).**

**You'll need your *Google Calendar Address* and a *Google API Key*.**

# The Code

Basically, the final code can be pulled apart into three main sections.

You have some JavaScript, then a div in html where the content will go, and then some more JavaScript.

Unfortunately, if you were looking for a JS-agnostic approach, I can't help you out.

## Section 1.0: JavaScript

So, for the first section of code, we're going to make a way to convert the date and time imprinted in the data returned by the API into human readable form. (Don't worry, we'll create an API call later, just do this first).

{% highlight html %}
<script type="text/javascript">
<!--
/**
 * Converts an xs:date or xs:dateTime formatted string into the local timezone
 * and outputs a human-readable form of this date or date/time.
 *
 * @param {string} gCalTime is the xs:date or xs:dateTime formatted string
 * @return {string} is the human-readable date or date/time string
 */
function formatGCalTime(gCalTime) {
  // text for regex matches
  var remtxt = gCalTime;

  function consume(retxt) {
    var match = remtxt.match(new RegExp('^' + retxt));
    if (match) {
      remtxt = remtxt.substring(match[0].length);
      return match[0];
    }
    return '';
  }

  // minutes of correction between gCalTime and GMT
  var totalCorrMins = 0;

  var year = consume('\\d{4}');
  consume('-?');
  var month = consume('\\d{2}');
  consume('-?');
  var dateMonth = consume('\\d{2}');
  var timeOrNot = consume('T');

  // if a DATE-TIME was matched in the regex
  if (timeOrNot == 'T') {
    var hours = consume('\\d{2}');
    consume(':?');
    var mins = consume('\\d{2}');
    consume('(:\\d{2})?(\\.\\d{3})?');
    var zuluOrNot = consume('Z');

    // if time from server is not already in GMT, calculate offset
    if (zuluOrNot != 'Z') {
      var corrPlusMinus = consume('[\\+\\-]');
      if (corrPlusMinus != '') {
        var corrHours = consume('\\d{2}');
        consume(':?');
        var corrMins = consume('\\d{2}');
        totalCorrMins = (corrPlusMinus=='-' ? 1 : -1) *
            (Number(corrHours) * 60 +
	    (corrMins=='' ? 0 : Number(corrMins)));
      }
    }

    // get time since epoch and apply correction, if necessary
    // relies upon Date object to convert the GMT time to the local
    // timezone
    var originalDateEpoch = Date.UTC(year, month - 1, dateMonth, hours, mins);
    var gmtDateEpoch = originalDateEpoch + totalCorrMins * 1000 * 60;
    var ld = new Date(gmtDateEpoch);

    // date is originally in YYYY-MM-DD format
    // time is originally in a 24-hour format
    // this converts it to MM/DD hh:mm (AM|PM)
    dateString = (ld.getMonth() + 1) + '/' + ld.getDate() + ' ' +
        ((ld.getHours()>12)?(ld.getHours()-12):(ld.getHours()===0?12:
	ld.getHours())) + ':' + ((ld.getMinutes()<10)?('0' +
	ld.getMinutes()):(ld.getMinutes())) + ' ' +
	((ld.getHours()>=12)?'PM':'AM');
  } else {
    // if only a DATE was matched
    dateString =  parseInt(month, 10) + '/' + parseInt(dateMonth, 10);
  }
  return dateString;
}

{% endhighlight %}

So, the above code attempts to convert the time into human-readable format, and then tries to convert it to GMT, and then into the local timezone of the Calendar.

It is roundabout - but still the most effective way I've found. If you can think of a shorter way, drop me a comment below.

## Section 1.1: JavaScript

{% highlight javascript %}
/**
 * Creates an unordered list of events in a human-readable form
 *
 * @param {json} root is the root JSON-formatted content from GData
 * @param {string} divId is the div in which the events are added
 */
function listEvents(root, divId) {
  var feed = root.feed;
  var events = document.getElementById(divId);

  if (events.childNodes.length > 0) {
    events.removeChild(events.childNodes[0]);
  }

  // create a new unordered list
  var ul = document.createElement('ul');

  // loop through each event in the feed
  for (var i = 0; i < feed.entry.length; i++) {
    var entry = feed.entry[i];
    var title = entry.title.$t;
    var start = entry['gd$when'][0].startTime;
    var finish = entry['gd$when'][0].endTime;
    var where = entry['gd$where'][0].valueString;

    // get the URL to link to the event
    for (var linki = 0; linki < entry['link'].length; linki++) {
      if (entry['link'][linki]['type'] == 'text/html' &&
          entry['link'][linki]['rel'] == 'alternate') {
        var entryLinkHref = entry['link'][linki]['href'];
      }
    }

    var dateString1 = formatGCalTime(start);
    var spacer = " till ";
    var dateString2 = formatGCalTime(finish);
    var dateString = dateString1.concat(spacer);
    var dateString = dateString.concat(dateString2)
    var li = document.createElement('li');
    var br = document.createElement('br');
    var h3 = document.createElement('h3');
    linker = document.createElement('a');

    // if we have a link to the event, create an 'a' element
    if (typeof entryLinkHref != 'undefined') {
      entryLink = document.createElement('a');
      entryLink.setAttribute('href', entryLinkHref);
      entryLink.appendChild(document.createTextNode(title));
      li.appendChild(br);
      h3.appendChild(entryLink);
      li.appendChild(br);
      li.appendChild(document.createTextNode(" " + dateString))
      li.appendChild(br);
      linker.setAttribute('href', 'https://www.google.com.au/maps/place/' + where);
      linker.appendChild(document.createTextNode(where));
      li.appendChild(linker);

    } else {
      li.appendChild(document.createTextNode(title));
      li.appendChild(br);
      li.appendChild(document.createTextNode(" " + dateString));
      li.appendChild(br);
      linker.setAttribute('href', 'https://www.google.com.au/maps/place/' + where);
      linker.appendChild(document.createTextNode(where));
      li.appendChild(linker);
    }

    // append the list item onto the unordered list
    ul.appendChild(h3);
    ul.appendChild(li);
    var br = document.createElement('br')
    ul.appendChild(br);
  }
  events.appendChild(ul);
}
{% endhighlight %}

This code is where the magic happens.

It grabs events from the calendar, hides them if they are in the past, and converts their "place" attribute into a Google Maps URL. (Something I find most people use all the time).

It also enables each event as a URL to the calendar, so when you click it, you get a chance to add the event to your own calendar.

##Section 1.3: JavaScript

The last part of this script:

{% highlight javascript %}
/**
 * Callback function for the GData json-in-script call
 * Inserts the supplied list of events into a div of a pre-defined name
 *
 * @param {json} root is the JSON-formatted content from GData
 */
function insertAgenda(root) {
  listEvents(root, 'agenda');
}
//-->
</script>
{% endhighlight %}

This small bit of code is utterly vital. It pushes all the data we just generated into the div itself.

Simple enough.

##Section 2: HTML
{% highlight html %}
<h2>Upcoming Events</h2>
<div id="agenda"><p>Loading...</p></div>
{% endhighlight %}

Simple enough as well. This piece of html creates an appropriate title, and then makes the div have the id we defined in Section 1.3, which you can change if you like. (Just make sure it changes in those two places).

##Section 3: JavaScript
{% highlight javascript %}
<script type="text/javascript" src="http://www.google.com/calendar/feeds/[Calendar Address]/public/full?alt=json-in-script&callback=insertAgenda&orderby=starttime&max-results=15&singleevents=true&sortorder=ascending&futureevents=true&key=[API Key]"></script>


    <script>

      devsite.github.Link.convertAnchors();

        window.prettyPrint();


    </script>
{% endhighlight %}

This is your last stage of code - and it's where we actually make the call to Google's API.

There are two sections in the above code that need modification to work for you:

* [Calendar Address]
* [API Key]

Why you need the calendar address is easy enough, and its location we found from the iframe before, however, why would you need an API Key?

To put it simply, Google's Public API gets millions of calls a second, and if you are under the same ISP as some of those, your call gets knocked back because of too many requests.

So, if you follow [this link](https://code.google.com/apis/console/?pli=1#:access), and get your own API Key, it should be successful. (There are limits to the number of requests you make, which means that if your website scales, you probably need to talk to the Google guys about being able to make more requests).

Again, for all that together, checkout the [gist](https://gist.github.com/shakna-israel/6d28ef500e16579e1639).
