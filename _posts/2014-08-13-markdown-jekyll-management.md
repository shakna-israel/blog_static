---
layout: post
title: Markdown, Management and Jekyll
date: 2014-08-13
excerpt: I've been using Jekyll for a while now, and I've become used to its quirks, but one of Jekyll's key features is it's ability to parse markdown into useable content. Like this blog is just a couple lines of code, and then markdown.
---

#Introduction

I've been using [Jekyll](http://jekyllrb.com) for a while now, and I've become used to its quirks, but one of Jekyll's key features is it's ability to parse markdown into useable content.

Like this blog is just a couple lines of code, and then markdown.

{% highlight ruby %}
---
layout: post
title: Markdown, Management and Jekyll
---
{% endhighlight %}

So based, on that, I set out to control the backgrounds on [Haddon Community Learning Centre](http://shakna-israel.github.io/haddonRevamp) in the same way.

And it worked.

# The YAML frontmatter

Each markdown page on HCLC has the following style of yml:
{% highlight ruby %}
---
layout: page
permalink: /news/
title: News
tags: [news, events, courses, woady, yaloak, herald, newspaper, recent]
modified: 2013-09-13
---
{% endhighlight %}

With the alteration, you now add another line, if you want to:
{% highlight ruby %}
---
layout: page
permalink: /news/
title: News
tags: [news, events, courses, woady, yaloak, herald, newspaper, recent]
modified: 2013-09-13
background: default.jpg
---
{% endhighlight %}

This allows you to select a page background image from the images directory, or if you leave it blank, it defaults.

How was the magic done?

* CSS
* Layout file

#CSS
{% highlight ruby %}
#bg `{`
  position: fixed;
  top: -50%;
  left: -50%;
  width: 200%;
  height: 200%;
  z-index:-400;
`}`
{% endhighlight %}

{% highlight ruby %}
#bg img `{`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  min-width: 50%;
  min-height: 50%;
  opacity: 0.5; /* For transparency */
  filter: alpha(opacity=50); /* For IE8 and earlier */
`}`
{% endhighlight %}

This CSS is fairly simple in the task it accomplishes. It takes an image in the div with the id of "bg" and throws it up as a background, and slightly transparent.

#Layout

Inside the _layouts/page.html, you introduce this:

{% highlight ruby %}
<div id="bg">
  <img src="{% if page.background %} {{ site.url }}/images/{{ page.background }} {% else %} {{ site.url }}/images/default.jpg {% endif %}" alt="">
</div>
{% endhighlight %}

It does depend on whether you use site.url or site.baseurl when using Jekyll, but that's an easy tweak for you.

Basically it checks if background is defined on the page, if it is, then display it, otherwise show the default. (Which isn't set in the config file, but would be easy to do that to.)
