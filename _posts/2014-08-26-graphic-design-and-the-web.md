---
layout: post
title: Graphic Design and the Web
date: 2014-08-26
excerpt: When it comes to website development, a lot of us forget graphic design in favour of working crazy magic with CSS...
---

When it comes to website development, a lot of us forget graphic design in favour of working crazy magic with CSS.

An example of this would by [/r/css](http://www.reddit.com/r/css). It's a place for experts and others in CSS to group together, show off and learn. But they've fallen into a trap we all fall into far too easily.

CSS is a useful tool - but it has its own place.

But graphic design is a tool that front-end web developers need to know and understand. Knowing the principles and rules of design can help with developing a website, but there are exceptions there. But graphic design fits a website.

This logo is what is used to tie my [design site](http://jmilne.com.au) together:

![jm | Design Logo](http://jmilne.com.au/img/profile.png)

Without it, the site isn't much more than a single-page site, because at this stage most single-page sites (that don't use Angular.js) look fairly similar.

So, ignoring a boring wall of text, here are some examples of amazing design out there, which don't use impressive scripts to do what a 500kb or less file can do:

![Emotio Design Group]({{ site.baseurl }}/images/emotiodesigngroup.jpg)
[View Site](http://www.emotio-design-group.co.uk/)

![Hochburg Design Agency]({{ site.baseurl }}/images/hochburgdesignagency.jpg)
[View Site](http://www.hochburg.net/de/)

![Gotham Chronicle]({{ site.baseurl }}/images/gothamchronicle.jpg)
[View Site](http://gothamchronicle.com/)

![Oui Will Agency]({{ site.baseurl }}/images/ouiwillagency.jpg)
[View Site](http://www.ouiwill.com/)
