---
layout: post
title: A Quick Guide to YAML
date: 2014-09-04
excerpt: YAML is a hell of a powerful language. It's website can scare most people away, and the recursive name can hurt as well (YAML stands for YAML Ain't Markup Language, and the YAML in that means the same, and so on and so forth). That being said, YAML makes my life easy.
---

[YAML](http://www.yaml.org/) is a hell of a powerful language.

It's website can scare most people away, and the recursive name can hurt as well (YAML stands for YAML Ain't Markup Language, and the YAML in that means the same, and so on and so forth).

That being said, YAML makes my life *easy*.

I use it in nearly every website I develop, to make things faster, easier and most of all *simple*.

Now, I don't have to use YAML. I could use [JSON](http://json.org) instead. There may be times and places JSON is more appropriate.

*Now let's get to it!*

* YAML is commonly used with [Jekyll](http://jekyllrb.com), or [Middleman](http://middlemanapp.com).

* Despite it's name, its most common usage is as *Markup*, a set of variables you put somewhere, at the start of a page usually.

So, to make things easy to understand, lets take a look at this page's markup, first in JSON, and then in YAML.

{% raw %}
```{```
  ```"layout": "post",```
  ```"title": "A Quick Guide to YAML",```
  ```"excerpt": null```
```}```
{% endraw %}

As you can see, the JSON is fairly easy to understand, and it is well-used throughout the world.

However... This is the same thing in YAML.

{% raw %}
```---```
```layout: post```
```title: A Quick Guide to YAML```
```excerpt:```
```---```
{% endraw %}

Some of the major benefits of YAML:

* Tabbing is not as sensitive as in JSON
* It can be easier to read when skimming
* Supported natively in Jekyll and Middleman
* If there is a network interrupt, YAML is still generally valid enough to display
* It is perfect for config files

There are drawbacks, but due to the speed of development, these might be different by the time you read this.

Mostly though, YAML is easy to write and read.

*And yes, YAML can be stored in files. ```*.yml``` by default*
