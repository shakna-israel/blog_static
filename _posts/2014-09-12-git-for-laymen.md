---
layout: post
title: Git For Laymen
date: 2014-09-12
excerpt: Git is not a tool that is hard to use, and it can be utterly invaluable. It can act as your backup system, automate website builds, and a heck of a lot more - but it can be scary.
---

Git is not a tool that is hard to use, and it can be utterly invaluable. It can act as your backup system, automate website builds, and a heck of a lot more - but it can be scary.

But it doesn't need to be.

This guide will make a few assumptions, before we get started.

###You have git installed.
* *See [here](https://help.github.com/articles/set-up-git) for a guide.*

###You know how to open the commandline, shell or whatever similar system your computer uses.
* *See [here](http://windows.microsoft.com/en-US/windows-vista/Open-a-Command-Prompt-window) for Windows.*
* *See [here](http://blog.teamtreehouse.com/introduction-to-the-mac-os-x-command-line) for Mac.*
* *Alt+F1 should open a fullscreen terminal on most Linux distributions... But most Linux users know how to use the terminal.*

So... Onwards!

##What is Git?

Git is a complicated little program that has quite a few features, but they are all based around the same idea.

**Git is a *Distributed Version Control System***

Basically, that means git is used to track changes and record them... It gets clearer once you actually use git... So, let's start.

##Making Your First Git Repository

1. Open your commandline equivalent, which I will call the *shell* from now on.
2. Lets make a folder, or *directory* to work on.

So, type the following, and then press enter. This will create a folder called *gitFolder* on your computer.

```mkdir gitFolder```

![mkdir gitFolder]({{ site.basicurl }}images/gitGuide/mkdir.PNG)

Next, we need to be able to run commands from inside that folder.

2: Change *directory* to gitFolder.

```cd gitFolder```

![cd gitFolder]({{ site.basicurl }}images/gitGuide/cd.PNG)

3: *Initialise Git* - Make git aware that it should be doing something with this folder.

```git init```

You should get a message like the one shown here:

![git init]({{ site.basicurl }}images/gitGuide/gitinit.PNG)

Git is now active, or *initialised*.

That being said... Git isn't tracking anything yet. The history it is tracking, or the *repository*, is **empty**.

4: Lets make some files!

On Windows, run ```copy /y nul file```

![copy /y nul file]({{ site.basicurl }}images/gitGuide/wintouch.PNG)

On Linux and Mac, run ```touch file```

![touch file]({{ site.basicurl }}images/gitGuide/touch.PNG)

This should make a file named, well... *file*.

*You can test this by running ```ls``` in Linux or Mac, and ```dir``` in Windows, and it should list the contents of the folder.*

5: Add files to your git *repository*

Now, in your *shell*, run ```git add .```

![git add .]({{ site.basicurl }}images/gitGuide/gitaddall.PNG)

This command will add all files to your git *repository*.

You can also add individual files similarly:

```git add filename```

6: Make git take a snapshot

Git stores snapshots of your files, and that's how you can move backwards in history when you need to.

These snapshots are called *commit*s.

So, let's make a *commit*!

```git commit -am "Added file!"```

![git add .]({{ site.basicurl }}images/gitGuide/gitcommit.PNG)

Now, when you need to look at git's history, the comment *Added file!* will help you know what version you want to use.

7: Viewing git history

So, we can see that we added *file* to git, and when, with this command:

```git log -p file```

![git add .]({{ site.basicurl }}images/gitGuide/gitlogp.PNG)

We can also view the history for all files with this command:

```git log```

8: Lets make some changes.

So, using the same way as before, lets create some new files.

*Git tracks changes in many types of files, as well as new files, or files being deleted.*

On Windows, run ```copy /y nul file2```

On Linux and Mac, run ```touch file2```

And now, let's commit these changes!

```git add .```

```git commit -am "Added file2!"

![git add .]({{ site.basicurl }}images/gitGuide/gitcommit2.PNG)

9: Now for something complicated - Let's undo a change!

Let's say that we didn't actually want to add *file2* to the *repository*.

We have two choices - remove it from the *repository*, or undo the commit.

The first is easy, the second *very difficult for a beginner to get the hang of*.

So, to remove *file2* from the *repository*:

```git rm file2```

And then commit it:

```git commit -m "Removed file2"```

**Wait!** - That was different.

Usually it would be:

```git commit -am "Something to say"```

The *a* has been omitted.

This is because the *a* usually *adds* files to git. But we're trying not to add one.

As for actually rolling back to a previous commit... You can see that sort of headache [here](https://stackoverflow.com/questions/927358/undo-the-last-git-commit), because it is well beyond the scope of the amateur.

10: Lets push it to a remote!

Git lets you have remotes, so that you can copy all your committed history out onto another computer. A remote is just a link to that other git *repository*.

Why would you want to do this?

Well, this website is hosted on git. You can see my code [here](https://github.com/shakna-israel/blog_static), which I use most of these commands on.

So, first, you need to set up a remote repository.

You have some choices here, but [GitHub](https://github.com) is the easiest. Set up an account, and create a repository. (See [here](https://help.github.com/articles/creating-a-new-repository) if you have trouble creating a repository.)

Now, go back to your *shell*.

This command will need you to put in a few things yourself.

* $username should be replaced by your username. In my case, it is *shakna-israel*.
* $repository should be replaced by whatever you named your repository when creating it on GitHub.
* $password should be replaced by your GitHub password.

Run the command:

```git remote add origin https://github.com/$username/$repository.git```

We've added a remote, named *origin*, for your repository!

Now, to *push* it to the other *repository*, we run this command:

```git push origin master```

*Master is used here, because its the default, but you can actually have *branches* in git, where multiple similar repositories sit near each other. Like a development and production version of a product, in which case the word *master* would change to the name of the branch.*

Now, if you open http://github.com/$username/$repository then you should be able to see your repository!

###Cloning

Cloning is where you copy a remote repository onto your own computer.

So, lets download a remote repository inside our gitFolder.

Run this command:

```git clone http://github.com/shakna-israel/osn.git```

This will *clone* my *repository* called *osn* into a folder called osn, inside gitFolder.

If you run ```dir``` on Windows, or ```ls``` on Linux or Mac, then you should see the folder.

You just downloaded a repository using the clone command!

But, clone doesn't just download.

If you clone a repository you have user rights to, such as your own repository, then you can simply start committing and pushing, without the need to set git up.
