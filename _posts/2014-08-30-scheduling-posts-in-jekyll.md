---
layout: post
title: Scheduling Posts in Jekyll
date: 2014-08-30
excerpt: If you have a blog, its something fundamental that you want to do. Write a post, upload, and forget. You don't want to have to manually build anything.
---

Updated *30th August, 2014*, due to a more compatible version.

If you have a blog, its something fundamental that you want to do. Write a post, upload, and forget. You don't want to have to manually build anything.

That being said, when I looked into how others were doing this... It was ridiculous.

Most people make use of rake statements, such as this:

```
rake post title="Some future post" date="2013-06-07"
```

Or they modify the _config.yml file to include new variables.

Personally, I thought that was stupid. It has to be easier... And it is.

I use paginator on this blog to make Jekyll push old posts to more pages.

So my whole front page looks like:

{% raw %}
```
<div class="posts">
  {% for post in paginator.posts %}
  {% capture post_date %}{{ post.date | date: '%s' }}{% endcapture %}
  {% capture site_time %}{{ site.time | date: '%s' }}{% endcapture %}
  {% if post_date < site_time %}
  <div class="post">
    <h1 class="post-title">
      <a href="{{ post.url }}">
        {{ post.title }}
      </a>
    </h1>

    <span class="post-date">{{ post.date | date_to_string }}</span>

    {{ post.excerpt }}...
  </div>
  {% endif %}
  {% endfor %}
</div>
```
{% endraw %}

The enabling of future posts is done with just a few lines of code.

Inside your *for post* loop, you have this:

{% raw %}

```{% capture post_date %}{{ post.date | date: '%s' }}{% endcapture %}```
```{% capture site_time %}{{ site.time | date: '%s' }}{% endcapture %}```
```{% if post_date < site_time %}```

{% endraw %}

This checks if the post's date is smaller than the current date. So, if the post's date is greater than the current server date, its ignored.

Then just before you end the *for post* loop, you just need to add the line:
{% raw %}

```{% endif %}```

{% endraw %}
That's it. Future posts are enabled.

*This is designed to run on a live Jekyll Server, such as provided by [GitHub Pages](http://pages.github.com). It may require tweaking, and an external API to make it run if you compile and upload HTML files.*

Once the post is older than the server, they appear. No extra effort required, no messing with config files or commands.
