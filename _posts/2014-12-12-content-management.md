## Content Management for Jekyll

I am a fan of [Jekyll](http://jekyllrb.com), and I also extensively use [Github](https://github.com).

As stated in previous posts, many of my clients allow me to use [Github Pages](https://github.io) to serve their websites.

It's a version of Jekyll that is hosted freely on Github, though one of it's limitations is the disabling of plugins.

However, editing files and then pushing them via git, is not something most clients want to dare think of.

They are far more used to WYSIWYG editors, and can panic a little if confronted even with a simple Markdown file.

However, there is a way to try and blend the two worlds.

The tool I want to draw attention to, is [Prose](http://prose.io).

It's designed primarily for content editing and writing, which suits most customers well, and connects to Github Pages well.

Prose has a few customisations of its own, which means you can enforce a rule so that clients can only upload images to the right folder, can't edit some files, and so on.

But why talk about it?

Load [Prose](http://prose.io) up now.