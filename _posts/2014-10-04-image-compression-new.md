---
layout: post
title: Batch Image Compression
date: 2014-09-12
excerpt: Image Compression is insanely important to website development. If images load slowly, so does your website...
---

Image Compression is insanely important to website development. If images load slowly, so does your website.

I've covered this [before](http://blog.shaknaisrael.com/2014/08/20/image-compression/), with one of my favourite tools, [compressor.io](//compressor.io).

[Compressor.io](//compressor.io) makes use of a series of Open Source tools, but doesn't *yet* allow for batch upload and download.

So, I set out to build a script that batch compresses all images in a folder, overwriting them, using the same tools.

**Note:** *This can take an incredible amount of time, depending on the speed and memory of your computer.*

Once finished, the script can automagickally compress, up to 70% (average), every ```*.jpg```, ```*.png```, ```*.gif```, and ```*.svg```.  

First: Lets install all the tools!

*All that follows happens on the commandline, and was meant for Linux systems. OSX, and Windows instructions may vary, see the linked websites for help.*

#jpegoptim

[Homepage](http://www.kokkonen.net/tjko/projects.html), [Repository](https://github.com/tjko/jpegoptim).

First, we need to download the latest version of jpegoptim.

This needs ```git``` to be installed. *On Debian based systems run ```apt-get install git```.*

Run: ```git clone https://github.com/tjko/jpegoptim.git```

This will create a folder.

Run: ```cd jpegopti*```

This opens the newly created folder.

Run: ```./configure```

This gets your system ready to build the application, so that you can install it.

Run: ```make```

This builds the application.

*Note, this next command may require you to use ```sudo```*

Run: ```make install```

This should install jpegoptim. (You can remove the folder now, if you want.)

#pngquant

[Homepage](http://pngquant.org/), [Repository](https://github.com/pornel/pngquant)

First, we need to download the latest version of pngquant.

This needs ```git``` to be installed. *On Debian based systems run ```apt-get install git```.*

Run: ```git clone https://github.com/pornel/pngquant.git```

This will create a folder.

Run: ```cd pngquan*```

This opens the newly created folder.

Run: ```./configure```

This gets your system ready to build the application, so that you can install it.

Run: ```make```

This builds the application.

*Note, this next command may require you to use ```sudo```*

Run: ```make install```

This should install pngquant. (You can remove the folder now, if you want.)

#optipng

[Homepage](http://optipng.sourceforge.net/), [Repository](http://sourceforge.net/projects/optipng/files/OptiPNG/)

Note: *This may not necessarily download the latest version.*

*You may need to install wget. On Debian, ```apt-get install wget``` should install it.*

Run: ```wget http://prdownloads.sourceforge.net/optipng/optipng-0.7.5.tar.gz?download```

This downloads a type of zip file.

Run: ```tar -zxvf optipng*```

This will create a folder.

Run: ```cd optipng*```

This opens the newly created folder.

Run: ```./configure```

This gets your system ready to build the application, so that you can install it.

Run: ```make```

This builds the application.

*Note, this next command may require you to use ```sudo```*

Run: ```make install```

This should install optipng. (You can remove the folder now, if you want.)

#gifsicle

[Homepage](http://www.lcdf.org/gifsicle/), [Repository](https://github.com/kohler/gifsicle).

First, we need to download the latest version of gifsicle.

This needs ```git``` to be installed. *On Debian based systems run ```apt-get install git```.*

Run: ```git clone https://github.com/kohler/gifsicle.git```

This will create a folder.

Run: ```cd gifsicle*```

This opens the newly created folder.

Run: ```./configure```

This gets your system ready to build the application, so that you can install it.

Run: ```make```

This builds the application.

*Note, this next command may require you to use ```sudo```*

Run: ```make install```

This should install gifsicle. (You can remove the folder now, if you want.)


#scour

*Scour is slightly different. It is not a program so much as it is a Python script. Therefore, Python must be installed.*

[Homepage](http://www.codedread.com/scour/).

Note: *This may not necessarily download the latest version.*

*You may need to install wget. On Debian, ```apt-get install wget``` should install it.*

Run: ```wget http://www.codedread.com/scour/scour-0.26.zip```

This downloads a type of zip file.

Run: ```unzip scour*```

This will create a folder.

This folder is the program. Do not allow it to be deleted.

(In the following script, this folder is found at ```~/scour```, you may need to change the script to match your install location).

#Finally, the Script

Finally, lets build the script.

```cd ~```

Now that we're back in the home directory, lets make a new file, which will house the script.

```nano image_compress.sh```

And fill it with this:

```
jpegoptim -t *.jpg
sleep 5s
jpegoptim --all-progressive *.jpg
sleep 5s
pngquant *.png
sleep 5s
optipng -o7 -strip all *.png
sleep 5s
gifsicle --batch -i *.gif
sleep 5s
_cwd="$PWD"
for file in $_cwd/*.svg
do
	python ~/scour/scour.py -i ${file} -o ${file}
done
```

The ```sleep 5s``` between each command is not strictly necessary, and makes it take 20s longer to run, but it can make it less likely to encounter issues.

*The line ```python ~/scour/scour.py -i ${file} ${file}``` may need to be changed to match where you placed the folder for scour.*

Press ```ctrl``` and ```x```, and then tap ```y```.

This saves the file.

#Using the Script

Due to the usage of python, thanks to scour, it would be unwise to make this file available for use everywhere.

So, instead, you should copy the script to the location you want to use it in, run it, and then delete it.

For example:

```cd assets/img```

```cp ~/image_compress.sh image_compress.sh```

```sh image_compress.sh```

```rm image_compress.sh```
