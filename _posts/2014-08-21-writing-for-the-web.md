---
layout: post
title: Writing For The Web
date: 2014-08-21
excerpt: When it comes to writing for the web, for many developers it is second nature. They've picked up tricks and tips amongst the years. However, for the new kids on the block, they often seem to have missed this lesson...
---

When it comes to writing for the web, for many developers it is second nature. They've picked up tricks and tips amongst the years. However, for the new kids on the block, they often seem to have missed this lesson.

So, lets hit the ground running.

First, if you are using English, (and choosing a language to write in is another topic), then you need to address your grammar.

##Grammar

**There are major grammatical differences between various versions of English.**

For example, here's a sentence which makes perfect sense coming from an Indian speaker:

```His insatiable thirst for knowledge accomplished him with all modern standards of scholarship.```

This makes perfect sense in Indian English.

However, it is *completely wrong* in American, British and Australian English. And will annoy any users coming from those locations.

They would prefer it be written more like this:

```
His insatiable thirst for knowledge meant that he accomplished all modern standard of scholarship.
```

Small difference - but key to many native English speakers.

**Next, grammatically, is the ```,```**

Many people might it pedantic to mention the comma, but it tells your readers when to breathe. The fewer you have, the more you miss them, the quicker you lose users, because they get bored.

**Most importantly, watch your word count.**

In high school and university (college) we learn to write academically, which usually means expanding you wordcount to meet the five thousand word requirement. This teaches us to waffle.

If you write for the web, twenty five words should be enough to make your point.

To illustrate this, here's two paragraphs of text that say the same thing:

```
In website development the developer looks for the simplest and most effective way to reach his audience with the same message. The fewer the words, the simpler the grammatical flow, the easier it is for users to read, and thus stay.
```

And now, shorter:

```
In website development it is best to look for a short and simple way to write, to retain users.
```

Same point, but the first paragraph is made up of 41 words, using 233 characters, whereas the second only uses 19 words, using 95 characters.

##Spelling

Simple rule of thumb: Never make a spelling mistake.

If you forget to spellcheck your site, or have an off day and miss one small mistake, it can have drastic consequences.

When a user notices a spelling mistake in the first third of the website, they leave. Immediately.

If the user has descended to the next third, then they will leave, but shortly. You have a chance to win their approval back. A chance.

If its even lower on the page, the user will leave, and will be unlikely to ever return to your website.

Make sure you check your spelling.

##Formatting

Formatting is easy.

Space your text.

This:

```This is more difficult to read because all the text runs into one line. It really is as simple as making this spaced.```

Is more difficult to read than:

```
This is much easier to read because all the text doesn't run into one line.

It really is as simple as making this spaced.
```

Make sure you know when to use ```one``` and ```1```.

Using numerical symbols can slow a user down and interrupt their flow. This can be good, if you want them to dwell on what they're looking at, or terrible if you interrupt them mid-sentence.
