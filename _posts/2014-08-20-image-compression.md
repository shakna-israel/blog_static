---
layout: post
title: Image Compression
date: 2014-08-20
excerpt: One of the major hurdles with web design, and has been since Web 2.0, is the fact that we use images. Users love images, icons replace the need...
---

One of the major hurdles with web design, and has been since Web 2.0, is the fact that we use *images*.

Users love images, icons replace the need for words in a lot of situations, and even if you attach a word, the icon often becomes what the repeat user sees, not the text underneath it.

Or images can simply 'tell a thousand words'.

**So what's the hurdle?** Images are supported easily enough, and most people have fast internet!

**The problem is that *not everyone* has fast internet**, and servers don't like loading large images, especially if you have ten images on a page.

#The Solution

For most web developers, they solve the issue by using *compression* to shrink file sizes down, hence the popularity of ```*.jpg``` and ```*.gif``` files. You may lose quality, but you make up for it in file size!

Another developer trick, mostly used on background images, is to *blur the image*. Remove the harsh lines, and it looks like a better background, but mostly, the file size can halve, or more. (Depending on the image of course).

That being said, compression tools are much preferred, and [*Stephane Lyver*](http://stephane.ly) stepped up with an easy-to-use tool, with a lot of grunt behind it.

#compressor.io

Harnessing some already-known compression utilities, such as pngquant, OptiPNG, JpegOptim, Gifsicle and Scour, he turned compressing an image nicely into a streamlined process, with his website [compressor.io](http://compressor.io).

Though bulk upload is a feature that is on its way, but not publicly available yet, this tool will probably take off.

I know several developers for whom using it is now second nature, but with my own experience of an average saving of 37% of the file size, whilst the official stats say the average saving is closer to 64%, whilst using lossless quality compression, this is definitely something you should trial run.
