---
layout: post
title: Re-booting
---


<div class="message">
  Rebooting this site took a bit more than it should have...
</div>

## Jekyll can be complicated

I thought throwing up a blog would be quick and simple, especially with the host supporting Jekyll out-of-box. It might not have any extensions, but I never planned to use any - and I haven't. (There's workarounds if you want extensions, however).

But...

Jekyll is incredibly pedantic.

Once I finally found a base theme I liked, Lanyon, I thought I'd just dump it in, and then build some new posts in markdown and it would all run up and quick.

That wasn't the case.

First issue was getting the paths to work correctly.

Basically, because I'm using gh-pages, this site has 'http://shakna-israel.github.io/blog_static' as the base path, however, on first installation, it was trying to use 'http://shakna-israel.github.io' as its base path.

I grabbed the YAML config file, dropped in a baseurl variable, and mass-changed all " href="/" " links to " {{ site.baseurl }}/ "

It didn't work.

That was unexpected.

Three hours later, the blog is up.

What follows, should be tips and tricks, and projects.
