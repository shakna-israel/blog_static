---
layout: page
title: Portfolio
---

<p class="message">
    Some of my works:
</p>

## Shakna Israel
<a href="http://shaknaisrael.com">
![Shakna Israel]({{ site.baseurl }}/images/shaknaisrael.png)
</a>

## John Diesel
<a href="http://johndiesel.com.au">
![John Diesel]({{ site.baseurl }}/images/johndiesel.png)
</a>

## JMilne.com.au
<a href="http://jmilne.com.au">
![JMilne.com.au]({{ site.baseurl }}/images/jmilne.png)
</a>

## Ballarat Christian College
<a href="http://balcc.vic.edu.au">
![Ballarat Christian College]({{ site.baseurl }}/images/balcc.png)
</a>

## Haddon Community Learning Centre
### Under Development
<a href="http://shakna-israel.github.io/haddon">
![Haddon Community Learning Centre]({{ site.baseurl }}/images/hclc.png)
</a>

<p class="message">
    Lots more from the whole team can be found on the <a href="http://www.auswideweb.com.au/?o=jmilne">work website</a>.
</p>
