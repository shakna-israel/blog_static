---
layout: page
title: About
---

<p class="message">
    And thus, did the code appear!
</p>

Before I found [Ruby](http://ruby-lang.org), I was still a web-developer - but I coded by hand.

I can barely believe the tedium I put up with, even if I was using a template.

Now?

Ruby speeds everything up, and makes everything easier to edit and build. (This site is written in Markdown, for example).

## James Milne

I'm a web developer over at [AusWideWeb](http://www.auswideweb.com.au/?o=jmilne), mostly working in HTML, but using Ruby as my backend.

Responsive Design is my focus, and my pet peeve. In the modern age, if you have a website, you need to make damn sure it will actually work - no matter how it is accessed.

## Blog - Technicals

This blog is a demonstration of the power of [Jekyll](http://jekyllrb.com) - and is hosted on GitHub's [gh-pages](http://github.io) project, so if you like it, you can grab it.

The theme is based on [Lanyon](http://lanyon.getpoole.com), a nice drop-in responsive design, built for [Poole](http://getpoole.com). (Though changes may be present on this site.)
